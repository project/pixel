<?php
?>
<li>
   <div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="sidebarbox block-<?php print $block->module ?>">
     <?php if ($block->subject): ?>
       <h2><?php print $block->subject ?></h2>
     <?php endif; ?>
   <?php print $block->content ?>
   </div>
 </li>
