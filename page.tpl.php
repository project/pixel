<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  
  <!--[if lt IE 8]>
    <link href="<?php  print base_path() . path_to_theme() ?>/ie.css" rel="stylesheet" type="text/css" />
  <![endif]-->

  <!--[if lt IE 7]>
    <link href="<?php  print base_path() . path_to_theme() ?>/ie6.css" rel="stylesheet" type="text/css" />
    <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE7.js" type="text/javascript"></script>
  <![endif]-->
</head>
<body class="<?php print $body_classes; ?>">
<div id="wrapper">

  <div id="header">

    <div id="logo">
      <h1><a href="<?php print $front_page; ?>"><?php print $site_name; ?></a></h1>
      <span><?php print $site_slogan; ?></span>
    </div>

    <div id="topright">
      <ul>
        <li><a href="#main">skip to content &darr;</a></li>
      </ul>
    </div>
    <div class="clear-block"></div>
  </div> <!-- Closes header -->

  <div id="catnav">
    <div id="toprss"><?php print $feed_icons ?></div> <!-- Closes toprss -->
    <?php print theme('links', $primary_links, array('class' => 'primarymenu', 'id' => 'nav')); ?>
  </div> <!-- Closes catnav -->

  <div class="clear-block"></div>

  <div id="main">

    <div id="contentwrapper">
       <?php if (!empty($breadcrumb)): ?><div class="drupal-breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
       <?php if (!empty($title) && empty($node)): ?><h2 class="topTitle"><?php print $title; ?></h2><?php endif; ?>
       <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
       <?php if (!empty($messages)): print $messages; endif; ?>
	   <?php if (!empty($help)): print $help; endif; ?>
       <?php if ($is_front && !empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
       <?php print $content; ?>
    </div> <!-- Closes contentwrapper-->

  <div id="sidebars">
    <?php if(!empty($right)): ?>
      <div id="sidebar_full">
        <ul>
          <?php print $right; ?>
        </ul>
      </div><!-- Closes Sidebar_full -->
    <?php endif; ?>

  <div class="clear-block"></div>
  </div> <!-- Closes Sidebars --><div class="clear-block"></div>

</div><!-- Closes Main -->

<?php if(!empty($footer)): ?>
  <div id="morefoot">
	<?php print $footer; ?>
    <div class="clear-block"></div>
  </div><!-- Closes morefoot -->
<?php endif; ?>

<div id="footer">
  <div id="footerleft">
    <p><?php print $footer_msg; ?> | <a href="http://samk.ca/freebies/" title="Pixel">Pixel</a> theme ported by <a href="http://topdrupalthemes.net">Drupal themes</a> | <a href="http://phpweby.com/hostgator_coupon.php">Hostgator coupon</a>. <a href="#main">Back to top &uarr;</a></p>
  </div>
  <div class="clear-block"></div>
</div><!-- Closes footer -->

</div><!-- Closes wrapper -->

<?php print $closure; ?>
</body>
</html>